package com.rydescape.map;

import java.util.HashMap;

public class Map {
    public static enum Index {
            LUMBRIDGE(3222, 3218),
            RIMMINGTON(2956, 3215),
            CATHERBY(2808, 3435),
            ENTRANA(2827, 3344),
            CAMELOT(2757, 3477),
            EDGEVILLE(3087, 3495),
            BARBARIAN_VILLAGE(3079, 3424),
            VARROCK(3207,3428),
            DRAYNOR(3109,3294),
            AL_KHARID(3293,3185),
            CRANDOR(2853,3237),
            KARAMJA(2953,3146),
            DUEL_ARENA(3365,3265),
            BRIMHAVEN(2814,3182),

            CASTLE_WARS(2454, 3089),
            PEST_CONTROL(2657, 2613),

            FELDIP_HILLS(2556, 2938),

            TEST_AREA(2207, 4959),
            KELDAGRIM(2887, 10224),
            TEST_AREA_3(2314, 4559),
            VERY_SMALL_TEST_AREA(2521, 9323),

            TEST_NPC_AREA(3435, 2955),

            SMALL_AREA_ALIVE(2633, 4700),
            SMALL_AREA_DEAD(2762, 4700),

            SMALL_TREE_RING(2602, 4773),
            SMALL_VILLAGE(2444, 5420),

            VARROCK_SEWERS(3237, 9858),
            EDGEVILLE_DUNGEON(3097, 9868),
            LUMBRIDGE_SWAMP(3190, 9569),


            WATER_ALTAR(2720, 4834),

            JAIL(2834, 10081),

            TUTORIAL_ISLAND_DUNGEON(3081, 9502),

        ;

        private int x;
        private int y;
        private int z;
        private static java.util.Map map = new HashMap<>();

        private Index(int x, int y) {
            this.x = x;
            this.y = y;
            this.z = 0;
        }

        private Index(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        static {
            for (Index _index : Index.values()) {
                int[] _values = new int[]{_index.x, _index.y, _index.z};
                map.put(_values, _index);
            }
        }

        public static Index valueOf(int _index) {
            return (Index) map.get(_index);
        }

        public int[] getValue() {
            return new int[]{this.x, this.y, this.z};
        }
    }
}
