package com.rydescape.item;

import java.util.HashMap;
import java.util.Map;

public class Item {
    public static enum Index {
        COINS(995),
        TINDERBOX(590),
        SMALL_FISHING_NET(303),
        RAW_SHRIMPS(317),
        SHRIMPS(315),
        COPPER_ORE(436),
        TIN_ORE(438),
        BONES(526),
        BRONZE_PLATELEGS(1075),
        BRONZE_PLATEBODY(1117),
        BRONZE_FULL_HELM(1155),
        WOODEN_SHIELD(1171),
        BRONZE_PICKAXE(1265),
        BRONZE_SWORD(1277),
        BRONZE_SCIMITAR(1321),
        BRONZE_AXE(1351),
        LOGS(1511),
        RAW_BEEF(2132),
        COOKED_MEAT(2142),
        BRONZE_BAR(2349),
        HAMMER(2347),
        NULL(592),
        ANCHOVIES(319),
        RAW_ANCHOVIES(321),
        BURNT_SHRIMP(323),
        BURNT_MEAT(2146),
        RAW_CHICKEN(2138),
        COOKED_CHICKEN(2140),
        BURNT_CHICKEN(2144),
        ANTI_DRAGON_SHIELD(1540),
        SLING(13199),


        SPIDER_CARCASS(6291),

        GHOSTLY_CAPE(6111),
        GHOSTLY_BOOT(6106),
        GHOSTLY_ROBE(6107),
        GHOSTLY_ROBE_BOTTOM(6108),
        GHOSTLY_HOOD(6109),
        GHOSTLY_GLOVES(6110),

        LOOTING_BAG(11941),
        ;

        private int value;
        private static Map map = new HashMap<>();

        private Index(int value) {
            this.value = value;
        }

        static {
            for (Index _index : Index.values()) {
                map.put(_index.value, _index);
            }
        }

        public static Index valueOf(int _index) {
            return (Index) map.get(_index);
        }

        public int getValue() {
            return value;
        }
    }
}
