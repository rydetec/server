package com.rydescape.item;

import java.util.Arrays;

import com.rydescape.item.Item.Index;

public class ItemConstants {
    public static Integer[] whitelist = {
        Index.COINS.getValue(),
        Index.TINDERBOX.getValue(),
        Index.SMALL_FISHING_NET.getValue(),
        Index.RAW_SHRIMPS.getValue(),
        Index.SHRIMPS.getValue(),
        Index.COPPER_ORE.getValue(),
        Index.TIN_ORE.getValue(),
        Index.BONES.getValue(),
        Index.BRONZE_PLATELEGS.getValue(),
        Index.BRONZE_PLATEBODY.getValue(),
        Index.BRONZE_FULL_HELM.getValue(),
        Index.WOODEN_SHIELD.getValue(),
        Index.BRONZE_PICKAXE.getValue(),
        Index.BRONZE_SWORD.getValue(),
        Index.BRONZE_SCIMITAR.getValue(),
        Index.BRONZE_AXE.getValue(),
        Index.LOGS.getValue(),
        Index.RAW_BEEF.getValue(),
        Index.COOKED_MEAT.getValue(),
        Index.BRONZE_BAR.getValue(),
        Index.HAMMER.getValue(),
        Index.ANCHOVIES.getValue(),
        Index.RAW_ANCHOVIES.getValue(),
        Index.RAW_BEEF.getValue(),
        Index.BURNT_MEAT.getValue(),
        Index.BURNT_SHRIMP.getValue(),
        Index.RAW_CHICKEN.getValue(),
        Index.COOKED_CHICKEN.getValue(),
        Index.BURNT_CHICKEN.getValue(),
        Index.ANTI_DRAGON_SHIELD.getValue(),
    };

    public static Integer[] commonPool = {
        Index.WOODEN_SHIELD.getValue(),
        Index.HAMMER.getValue(),
    };

    public static Integer[] uncommonPool = {
        Index.BRONZE_SWORD.getValue(),
        Index.BRONZE_AXE.getValue(),
        Index.TINDERBOX.getValue(),
        Index.SMALL_FISHING_NET.getValue(),
        Index.BRONZE_PICKAXE.getValue(),
    };

    public static Integer[] sparsePool = {
        Index.LOGS.getValue(),
        Index.COPPER_ORE.getValue(),
        Index.TIN_ORE.getValue(),
        Index.RAW_SHRIMPS.getValue(),
    };

    public static Integer[] valuablePool = {
        Index.BRONZE_BAR.getValue(),
        Index.SHRIMPS.getValue(),
    };

    public static Integer[] rarePool = {
        Index.BRONZE_FULL_HELM.getValue(),
        Index.BRONZE_PLATELEGS.getValue(),
        Index.BRONZE_PLATEBODY.getValue(),
    };

    public static Integer[] uniquePool = {
        Index.BRONZE_SCIMITAR.getValue(),
        Index.BRONZE_FULL_HELM.getValue(),
    };

    public static int nullId() {
        return Index.NULL.getValue();
    }

    public static boolean isAllowed(int id) {
        if (Arrays.asList(whitelist).contains(id)) {
            return true;
        }
        if (Arrays.asList(whitelist).contains(id - 1)) {
            return true;
        }
        return false;
    }

    public static int randomId() {
        double firstRoll = Math.random();
        double secondRoll = Math.random();
        int outOf100 = (int) (firstRoll * 100.0);

        if (firstRoll == secondRoll) {
            double thirdRoll = Math.random();
            return whitelist[(int) Math.round(thirdRoll * (double) (whitelist.length - 1))];
        }

        Integer[] pool = null;

        if (outOf100 <= 3) {
            pool = uniquePool;
        } else if (outOf100 <= 6) {
            pool = rarePool;
        } else if (outOf100 <= 12) {
            pool = valuablePool;
        } else if (outOf100 <= 25) {
            pool = sparsePool;
        } else if (outOf100 <= 50) {
            pool = uncommonPool;
        } else {
            pool = commonPool;
        }

        if (pool != null) {
            return pool[(int) Math.round(secondRoll * (double) (pool.length - 1))];
        }

        return commonPool[(int) Math.floor(Math.random() * (commonPool.length - 1))];
    }
}
