package com.rydescape;

import java.util.HashMap;
import java.util.Map;
public class RydeConstants {

    /**
	 * Checks if in verbose mode
	 */
    public static boolean VERBOSE_MODE = false;

    // Boosted exp rate
    public static double BOOSTED_RATE = 3.33;
    
    // Experiments
    public static enum EXPERIMENTS {
            NEW_SKILL_WRAPPER(false),
            SKIP_TUTORIAL(true),
            BOOSTED_ACCOUNT(true),
            BASIC_ECONOMY(true),
            BASIC_MOBS(true),
            NEW_POUCH_PAYMENT(true),
            DISABLE_TRIVIA_BOT(true),
            DISABLE_CUSTOM_OBJECT_SPAWNS(true),
            FREE_MEMBERSHIP(true),
            DISABLE_TRADABLE_ITEMS(true),
            LIMITED_UI(true),
            DISABLE_EXTRA_DROPS(true),
            BOOSTED_DROP_RATE(false),
            ALL_DROP_TABLE_ROLLS(false),
            RUN_DRAIN_CHANGES(true),
            DISABLE_SQL_METHODS(true),
            GREATER_WALKING_DISTANCE(true),
        ;

        private boolean value;
        private static Map map = new HashMap<>();

        private EXPERIMENTS(boolean value) {
            this.value = value;
        }

        static {
            for (EXPERIMENTS _index : EXPERIMENTS.values()) {
                map.put(_index.value, _index);
            }
        }

        public boolean getValue() {
            return value;
        }

        public void setValue(boolean _value) {
            this.value = _value;
        }
     }
}
