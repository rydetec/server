package com.rydescape.mob;

import java.util.HashMap;
import java.util.Map;

public class Mob {
    public static enum Index {
            MAN(3078),

            MINER(1992),

            BANKER(394),
            BANKER_2(2119),
            BANKER_3(2633),
            BANKER_4(3318),
            BANKER_5(397),

            
            TOWN_CRIER(278),

            COW(2805),
            COW_CALF(2807),
            COW_2(2808),
            COW_3(2806),

            SHOP_KEEPER(518),
            SHOP_KEEPER_2(510),
            SHOP_KEEPER_3(516),
            SHOP_KEEPER_4(506),
            SHOP_ASSISTANT(507),

            CUSTOMER(2372),

            SHRIMP_FISHING_SPOT(1518),
            NULL(24),
            
            
            ROCK_CRAB(100),

            

            GREEN_DRAGON(260),
            
            BRONZE_DRAGON(270),
            BRONZE_DRAGON_2(271),


            IRON_DRAGON(272),

            FIEND_ICE(4813),
            
            
            // Man
            MAN_CAPE_AMMY(385),
            MAN_ROUGH(1118),
            MAN_ROUGH_FAT(1138),
            MAN_SUIT(3014),
            MAN_BLUE(3078),
            MAN_BROWN(3079),
            MAN_GREEN(3080),
            MAN_BACKPACK_STICK(3081),
            MAN_BLACK(3101),
            MAN_FANCY_HAT(3109),
            MAN_STRANDED(3652),
            MAN_PINK_HAT(3265),
            MAN_BLUE_HAT(3264),
            MAN_BACKPACK_SUIT(3266),

            // Pets
            DOG(111),
            DOG_STRAY(3829),
            DOG_STRAY_2(2922),
            
            CUTE_CREATURE(1243),
            EXPERIMENT(1274),

            SNAKELING_2(2128),
            SNAKELING(2129),
            SNAKELING_RED(2131),
            SNAKELING_GREEN(2130),

            SLAVE(4541),
            KITTEN(3498),
            CREATURE_EVIL(1241),
            TOY_MOUSE(2781),

            // Goblin
            GOBLIN(3031),
            GOBLIN_2(3029),
            GOBLIN_3(3039),
            GOBLIN_RED_STAFF(656),
            GOBLIN_RED_SPEAR(658),
            GOBLIN_RED_AXE(655),
            GOBLIN_STAFF(668),
            GOBLIN_GREEN(677),
            GOBLIN_GREEN_2(664),
            GOBLIN_BROWN(3074),
            GOBLIN_GREEN_3(662),
            GOBLIN_BROWN_2(3075),
            GOBLIN_GREY(3041),
            GOBLIN_GREY_SPEAR(2484),
            GOBLIN_RED_HAMMER(657),
            GOBLIN_RED_AXE_2(659),
            GOBLIN_4(3038),
            GOBLIN_5(3043),
            GOBLIN_6(3052),
            GOBLIN_7(2488),
            GOBLIN_8(3044),
            GOBLIN_9(3045),
            GOBLIN_10(3073),
            GOBLIN_11(661),
            GOBLIN_12(3047),
            GOBLIN_GREEN_4(2487),
            GOBLIN_13(3053),
            GOBLIN_14(2249),
            GOBLIN_15(3028),
            GOBLIN_16(3037),
            GOBLIN_GREEN_5(666),
            GOBLIN_17(2246),

            // Animals
            GRIZZLY_BEAR(3423),
            GRIZZLY_BEAR_BABY(3424),
            GRIZZLY_BEAR_2(2838),

            BEAR_BLACK(2839),

            GIANT_RAT(2857),
            GIANT_RAT_2(2858),
            GIANT_RAT_3(2862),
            GIANT_RAT_4(2864),
            GIANT_RAT_5(2867),
            GIANT_RAT_6(3314),
            GIANT_RAT_7(2863),
            GIANT_RAT_8(2511),
            GIANT_RAT_9(3313),
            GIANT_RAT_10(2860),
            GIANT_RAT_11(3315),

            PIGLET(2814),
            PIGLET_2(2825),
            PIG(2823),
            PIG_2(2812),

            COW_BROWN(2810),

            BUNNY(3903),
            BUNNY_2(3902),
            RABBIT(3422),

            RACOON(1420),

            WOLF(110),
            WOLF_2(116),
            WOLF_DESERT(4650),
            WOLF_3(2491),
            WOLF_4(117),
            WOLF_5(3912),
            WOLF_DESERT_2(4649),
            WOLF_DESERT_3(4651),
            WOLF_6(231),
            WOLF_7(2490),
            WOLF_JUNGLE(232),

            HOUND_VAMPRIC(3238),

            SPIDER(3235),
            SPIDER_1(3019),
            SPIDER_2(4561),
            SPIDER_GIANT(3018),
            SPIDER_GIANT_2(3017),

            SNAKE_DESERT(3544),
            SNAKE(3545),
            SNAKE_1(3199),

            RAT(4611),
            RAT_2(4614),
            RAT_3(1022),
            RAT_4(4616),
            RAT_5(4612),
            RAT_6(2492),
            RAT_DUNGEON(3608),
            RAT_7(4615),
            RAT_8(4613),
            RAT_9(4594),
            RAT_DUNGEON_2(3609),
            RAT_10(1020),
            RAT_11(2513),
            RAT_DUNGEON_3(3607),
            RAT_DUNGEON_4(2866),
            RAT_12(4610),
            RAT_13(2855),

            ROOSTER(2818),
            ROOSTER_2(3663),

            SHEEP(2801),
            SHEEP_2(2796),
            SHEEP_SHEARED(2785),
            SHEEP_SHEARED_2(2787),
            SHEEP_SHAGGY(2803),
            SHEEP_SHEARED_3(2793),
            SHEEP_3(2800),
            SHEEP_4(2799),
            SHEEP_SHEARED_4(2786),
            SHEEP_SHEARED_5(2791),
            SHEEP_5(2804),

            SEAGULL(1554),
            
            FISH(4835),

            CHICKEN(2692),
            CHICKEN_2(2819),
            CHICKEN_3(2821),
            CHICKEN_4(2693),
            CHICKEN_5(2820),
            CHICKEN_6(3316),

            CAT(1622),
            CAT_WHITE(1620),
            CAT_ORANGE(1621),
            
            SQUIRREL(1418),
            SQUIRREL_2(1416),

            LIZARD_SWAMP(2906),
            LIZARD_SMALL(462),
            LIZARD_DESERT(461),

            MONKEY(1469),
            MONKEY_1(3200),
            MONKEY_2(2848),

            PENGUIN(233),
            PENGUIN_2(2063),

            GOAT(1793),
            GOAT_2(1792),

            FERRET(1505),

            CROW(2074),

            BAT(2827),

            BUTTERFLY(235),

            SCORPION_KING(3027),
            SCORPION(2479),
            SCORPION_PIT(3026),
            SCORPION_2(3024),
            SCORPION_3(2480),

            // Guard
            GUARD(3094),
            GUARD_2(1004),
            GUARD_1(3269),
            GUARD_RANGED(3273),
            GUARD_VARROCK(3011),
            GUARD_VARROCK_1(3010),
            GUARD_3(1000),
            GUARD_4(1001),
            GUARD_TOWN(3931),
            GUARD_5(1007),
            GUARD_6(3271),
            GUARD_7(1008),
            GUARD_8(999),

            // Hill Giants
            HILL_GIANT_1(2100),
            HILL_GIANT(2098),
            HILL_GIANT_2(2099),
            HILL_GIANT_3(2101),


            // Skeleton
            SKELETON_UNARMED(76),
            SKELETON(3972),
            SKELETON_2(3973),
            SKELETON_3(79),
            SKELETON_4(1686),
            SKELETON_5(2526),
            SKELETON_6(82),
            SKELETON_7(2520),
            SKELETON_8(78),
            SKELETON_HEADLESS(2523),
            SKELETON_HEADLESS_2(73),
            SKELETON_9(72),
            SKELETON_10(71),
            SKELETON_11(83),
            SKELETON_12(2522),
            SKELETON_13(924),
            SKELETON_14(81),
            SKELETON_15(70),
            SKELETON_16(80),
            SKELETON_17(1685),
            SKELETON_18(77),

            FARMER(3086),
            FARMER_2(3091),
            FARMER_MASTER(3257),
            FARMER_3(3672),
            FARMER_4(3087),
            FARMER_5(3089),

            ICE_GIANT(2087),

            MINOTAUR(2481),

            GHOST(87),
            GHOST_2(85),
            GHOST_3(2531),
            GHOST_4(3621),
            GHOST_5(3978),
            GHOST_6(2918),
            GHOST_7(1786),
            GHOST_8(3975),
            GHOST_9(2527),
            GHOST_10(3979),
            GHOST_11(89),
            GHOST_12(97),
            GHOST_13(2534),
            GHOST_14(90),
            GHOST_15(3620),
            GHOST_16(92),
            GHOST_17(3625),
            GHOST_18(95),

            ZOMBIE(37),
            ZOMBIE_2(40),
            ZOMBIE_3(2503),
            ZOMBIE_4(60),
            ZOMBIE_5(2508),
            ZOMBIE_6(2502),
            ZOMBIE_7(38),
            ZOMBIE_8(30),
            ZOMBIE_9(48),
            ZOMBIE_10(32),
            ZOMBIE_11(36),
            ZOMBIE_12(26),
            ZOMBIE_13(2506),
            ZOMBIE_14(39),
            ZOMBIE_15(54),
            ZOMBIE_16(42),
            ZOMBIE_17(46),
            ZOMBIE_18(57),
            ZOMBIE_19(51),
            ZOMBIE_20(2501),
            ZOMBIE_21(29),
            ZOMBIE_22(67),
            ZOMBIE_23(47),
            ZOMBIE_24(52),
            ZOMBIE_25(45),
            ZOMBIE_26(27),
            ZOMBIE_27(41),
            ZOMBIE_28(58),
            ZOMBIE_29(35),
            ZOMBIE_30(44),
            ZOMBIE_31(63),
            ZOMBIE_32(2505),
            ZOMBIE_33(59),
            ZOMBIE_34(28),
            ZOMBIE_35(64),
            ZOMBIE_36(50),
            ZOMBIE_37(65),
            ZOMBIE_38(66),
            ZOMBIE_39(3981),
            ZOMBIE_40(1784),
            ZOMBIE_41(31),
            ZOMBIE_42(61),
            ZOMBIE_43(2507),
            ZOMBIE_44(55),

            BARBARIAN(3062),
            BARBARIAN_2(3061),
            BARBARIAN_3(3055),
            BARBARIAN_4(3059),
            BARBARIAN_5(3064),
            BARBARIAN_6(3057),
            BARBARIAN_7(3072),
            BARBARIAN_8(3069),
            BARBARIAN_9(3058),
            BARBARIAN_10(3060),
            BARBARIAN_11(3102),
            BARBARIAN_12(3070),
            BARBARIAN_13(3067),
            BARBARIAN_14(3071),
            BARBARIAN_15(3066),

            DWARF(1403),
            DWARF_2(296),
            DWARF_3(294),
            DWARF_4(1407),
            DWARF_5(1401),

            HOBGOBLIN(3049),
            HOBGOBLIN_2(2241),
            HOBGOBLIN_3(3050),

            // Uniques
            KING_ROALD(4162),
            KING_ROALD_2(4163),
            LUMBRIDGE_GUIDE(306),
            HERO(3106),
            COUNT_DRAYNOR(3401),
            VANNAKA(403),

            // Bosses
            ZULRAH(2044),
            ZULRAH_2(2042),
            ZULRAH_3(2043),

            WIZARD(1160),
            WIZARD_1(3097),
            WIZARD_2(4400),
            WIZARD_DARK(2058),
            WIZARD_DARK_2(2057),

            MONK(4246),
            MONK_2(5279),
            MONK_3(4566),
            MONK_4(1171),

            DRAGON_BABY(245),

            VAMPIRE(3237),

            CAVE_BUG(481),
            CAVE_BUG_1(483),
            CAVE_CRAWLER(409),

            HIGHWAYMAN(2876),
            HIGHWAYMAN_2(2877),

            MUGGER(2871),

            CRAWLING_HAND(448),

            ROCK_CRAB_2(102),

            ANIMATED_ARMOR_IRON(2451),
            ANIMATED_ARMOR_BRONZE(2450),

            MIST(4435),

            WITCH(2873),
            WITCH_2(3566),
            WITCH_3(4778),
            WITCH_4(2872),
            WITCH_5(4409),

            MAKE_OVER_MAGE(1306),

            SANDSTORM(912),
            STORM_CLOUD(488),

            FISHERMAN(4934),

            WARRIOR_AL_KHARID(3103),
            WARRIOR_ICE(2842),

            ROUGE(2884),
        ;

        private int value;
        private static Map map = new HashMap<>();

        private Index(int value) {
            this.value = value;
        }

        static {
            for (Index _index : Index.values()) {
                map.put(_index.value, _index);
            }
        }

        public static Index valueOf(int _index) {
            return (Index) map.get(_index);
        }

        public int getValue() {
            return value;
        }

        public static int getSize() {
            return map.size();
        }
    }
}
