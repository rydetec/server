package com.rydescape.mob;

import java.util.Arrays;

import com.rydescape.mob.Mob.Index;

public class MobConstants {
    public static Integer[] whitelist = {
        Index.MAN.getValue(),
        Index.BANKER.getValue(),
        Index.GUARD.getValue(),
        Index.COW.getValue(),
        Index.SHOP_KEEPER.getValue(),
        Index.SHRIMP_FISHING_SPOT.getValue(),
        Index.NULL.getValue(),
        Index.SHEEP.getValue(),
        Index.CHICKEN.getValue(),
        Index.GOBLIN.getValue(),
        //Index.ROCK_CRAB.getValue(),
        //Index.HILL_GIANT.getValue(),
        //Index.SKELETON.getValue(),
        //Index.BRONZE_DRAGON.getValue(),
        
        2513, 2855, 2857, 2858, 3314, 3017, 37, 48, 3981, 2503, 76, 70, 77, 3049, 3050, 2098, 2101, 2099, 2100, 92, 90, 89, 87, 4561
    };

    public static Integer[] randomPool = {
        Index.MAN.getValue(),
        Index.GUARD.getValue(),
    };

    public static boolean isAllowed(int id) {
        if (Arrays.asList(whitelist).contains(id)) {
            return true;
        }
        return false;
    }

    public static boolean isIndexed(int id) {
        for (int i = 0; i < Mob.Index.values().length; i++) {
            if (Mob.Index.values()[i].getValue() == id) {
                return true;
            }
        }
        return false;
    }

    public static int randomId() {
        return Index.NULL.getValue(); //randomPool[(int) Math.floor(Math.random() * (randomPool.length - 1))]; //
    }
}
