package com.rydescape.user;

import com.rydescape.skill.Skill;

public class Character {
    Skill[] skills;

    public Character() {
        skills = new Skill[Skill.count];
        for (int i = 0; i < skills.length; i++) {
            if (i == Skill.Index.HITPOINTS.getValue()) {
                skills[i] = new Skill(Skill.Index.HITPOINTS, 10);
            }
            skills[i] = new Skill(Skill.Index.valueOf(i));
        }
    }

    public boolean addExperience(Skill.Index _skill, double _experience) {
        return skills[_skill.getValue()].addExperience(_experience);
    }

    public double getExperience(Skill.Index _skill) {
        return skills[_skill.getValue()].getExperience();
    }

    public int getLevel(Skill.Index _skill) {
        return skills[_skill.getValue()].getLevel();
    }

    public int getValue(Skill.Index _skill) {
        return skills[_skill.getValue()].getValue();
    }

    public void setValue(Skill.Index _skill, int _value) {
        skills[_skill.getValue()].setValue(_value);
    }

    public boolean levelUp(Skill.Index _skill) {
        return skills[_skill.getValue()].levelUp();
    }

    public boolean lowerValue(Skill.Index _skill, int _amount) {
        return skills[_skill.getValue()].lowerValue(_amount);
    }

    public boolean setExperience(Skill.Index _skill, double _amount) {
        return skills[_skill.getValue()].setExperience(_amount);
    }
}