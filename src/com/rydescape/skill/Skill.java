package com.rydescape.skill;

import java.util.HashMap;
import java.util.Map;

public class Skill {
    public static int count = 25; 

    public static final int[] MILESTONES = { 0, 83, 174, 276, 388, 512, 650, 801, 969, 1154, 1358,
		1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470, 5018, 5624, 6291, 7028, 7842,
		8740, 9730, 10824, 12031, 13363, 14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408,
		33648, 37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014, 91721, 101333,
		111945, 123660, 136594, 150872, 166636, 184040, 203254, 224466, 247886, 273742, 302288,
		333804, 368599, 407015, 449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257,
		992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808, 1986068, 2192818, 2421087,
		2673114, 2951373, 3258594, 3597792, 3972294, 4385776, 4842295, 5346332, 5902831,
		6517253, 7195629, 7944614, 8771558, 9684577, 10692629, 11805606, 13034431 };

    public static enum Index {
        ATTACK(0),
        DEFENCE(1),
        STRENGTH(2),
        HITPOINTS(3),
        RANGED(4),
        PRAYER(5),
        MAGIC(6),
        COOKING(7),
        WOODCUTTING(8),
        FLETCHING(9),
        FISHING(10),
        FIREMAKING(11),
        CRAFTING(12),
        SMITHING(13),
        MINING(14),
        HERBLORE(15),
        AGILITY(16),
        THIEVING(17),
        SLAYER(18),
        FARMING(19),
        RUNECRAFTING(20),
        SUMMONING(21),
        HUNTER(22),
        CONSTRUCTION(23),
        DUNGEONEERING(24);

        private int value;
        private static Map map = new HashMap<>();

        private Index(int value) {
            this.value = value;
        }

        static {
            for (Index _index : Index.values()) {
                map.put(_index.value, _index);
            }
        }

        public static Index valueOf(int _index) {
            return (Index) map.get(_index);
        }

        public int getValue() {
            return value;
        }
    }

    public static final String[] NAMES = { "attack", "defence", "strength", "hitpoints", "ranged", "prayer", "magic", "cooking", "woodcutting", "fletching", "fishing", "firemaking", "crafting", "smithing", "mining", "herblore", "agility", "thieving", "slayer", "farming", "runecrafting", "summoning", "hunter", "construction", "dungeoneering" };

    public static Index getIndex(int _index) {
        return Index.valueOf(_index);
    }

    Index index;
    int value = 1;
    int level = 1;
    double experience = 0.0;

    public int[] RATES = new int[count];

	public Skill(Index _index) {
        this.index = _index;
        
        for (int i = 0; i < count; i++) {
            RATES[i] = 1;
        }
    }
    
    public Skill(Index _index, int _level) {
        this.index = _index;
        this.level = _level;
        this.value = _level;
        this.experience = MILESTONES[_level - 1];

        for (int i = 0; i < count; i++) {
            RATES[i] = 1;
        }
    }

    public boolean addExperience(double _experience) {
        this.experience += _experience * RATES[this.index.getValue()];
        return true;
    }

    public double getExperience() {
        return this.experience;
    }

    public int getLevel() {
        return this.level;
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int _value) {
        this.value = _value;
    }

    public boolean levelUp() {
        boolean didLevel = false;
        while (this.experience >= MILESTONES[this.level]) {
            this.level++;
            this.value++;

            if (this.level >= 99) {
                this.level = 99;
                break;
            }
            didLevel = true;
        }
        return didLevel;
    }

    public boolean lowerValue(int _amount) {
        this.value -= _amount;
        if (this.value < 0) {
            this.value = 0;
        }
        return true;
    }

    public boolean setExperience(double _amount) {
        this.experience = _amount;
        this.levelUp();
        return true;
    }

}
