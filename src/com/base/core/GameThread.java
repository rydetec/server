package com.base.core;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import javax.swing.JTextArea;


import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.ChannelException;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import com.base.GameDataLoader;
import com.base.BaseConstants;
import com.base.core.network.PipelineFactory;
import com.base.core.task.TaskQueue;
import com.base.core.util.LineCounter;
import com.base.core.util.Stopwatch;
import com.base.core.util.SystemLogger;
import com.base.rs2.entity.World;
import com.base.rs2.entity.item.impl.GroundItemHandler;
import com.base.rs2.entity.object.ObjectManager;

public class GameThread extends Thread {
	
	/** The logger for printing information. */
	private static Logger logger = Logger.getLogger(GameThread.class.getSimpleName());

	public static JTextArea jta;

	public static boolean shouldStop = false;
	public static boolean dataLoaded = false;

	public static ServerBootstrap serverBootstrap;

	public static void init() {
		try {
			/**
			 * Load data, bind ports, init connections, launch worker threads
			 */
			try {
				startup();
			} catch (Exception e) {
				e.printStackTrace();
			}

			/**
			 * Begin game thread
			 */
			new GameThread();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Starts the server up.
	 * 
	 * @throws Exception
	 */
	private static void startup() throws Exception {
		// The stopwatch for timing how long all this takes.
		Stopwatch timer = new Stopwatch().reset();

		printToGUI("Launching RydeScape..");

		if (!BaseConstants.DEV_MODE) {
			System.setErr(new SystemLogger(System.err, new File("./data/logs/err")));
		}

		if (BaseConstants.DEV_MODE) {
			LineCounter.run();
		}

		

		if (!GameDataLoader.loaded()) {
			printToGUI("Loading game data..");
			GameDataLoader.load();
		}

//		logger.info("Loading character backup strategy..");
//		TaskQueue.queue(new PlayerBackupTask());

		while (!GameDataLoader.loaded()) {
			Thread.sleep(200);
		}

		printToGUI("Binding port and initializing threads..");

		serverBootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));
		serverBootstrap.setPipelineFactory(new PipelineFactory());

		/**
		 * Initialize worker threads
		 */
		new LoginThread();
		new NetworkThread();

		int portOff = 0;

		while (true) {
			try {
				serverBootstrap.bind(new InetSocketAddress(43594 + portOff));
				break;
			} catch (ChannelException e2) {
				printToGUI("Server could not bind port - sleeping..");
				portOff += 1;
				Thread.sleep(2000);
			}
		}

		shouldStop = false;

		printToGUI("Server successfully launched. [Took " + timer.elapsed() / 1000 + " seconds]");
	}

	public static void printToGUI(String _text) {
		if (jta != null) {
			jta.setText(_text + "\n" + jta.getText());
		}
		logger.info(_text);
	}

	/**
	 * Create the game thread
	 */
	private GameThread() {
		setName("Main Thread");
		setPriority(Thread.MAX_PRIORITY);
		start();
	}

	/**
	 * Performs a server cycle.
	 */
	private void cycle() {
		try {
			TaskQueue.process();
			GroundItemHandler.process();
			ObjectManager.process();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			World.process();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				long s = System.nanoTime();

				/**
				 * Cycle game
				 */
				cycle();

				if (shouldStop) {
					printToGUI("Shutting down server...");
					GameThread.serverBootstrap.shutdown();
					this.interrupt();
				}
				

				/**
				 * Sleep
				 */
				long e = (System.nanoTime() - s) / 1_000_000;

				/**
				 * Process incoming packets consecutively throughout the
				 * sleeping cycle *The key to instant switching of equipment
				 */
				if (e < 600) {

					if (e < 400) {
						for (int i = 0; i < 30; i++) {
							long sleep = (600 - e) / 30;
							Thread.sleep(sleep);
						}
					} else {
						Thread.sleep(600 - e);
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		printToGUI("Server halted.");
	}

}
