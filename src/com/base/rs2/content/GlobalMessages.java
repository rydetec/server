package com.base.rs2.content;

import java.util.ArrayList;
import java.util.logging.Logger;

import com.base.core.task.Task;
import com.base.core.task.TaskQueue;
import com.base.core.util.Utility;
import com.base.rs2.entity.World;
import com.rydescape.RydeConstants;

/**
 * Handles the global messages
 * @author Daniel
 *
 */
public class GlobalMessages {
	
	/**
	 * The logger for the class
	 */
	private static Logger logger = Logger.getLogger(GlobalMessages.class.getSimpleName());

	/**
	 * The news color text
	 */
	private static String newsColor = "<col=013B4F>";

	/**
	 * The news icon
	 */
	private static String iconNumber = "<img=8>";
	
	/**
	 * Holds all the announcements in a arraylist
	 */
	public final static ArrayList<String> announcements = new ArrayList<String>();

	/**
	 * The random messages that news will send
	 */
	public static final String[] ANNOUNCEMENTS = { 
		"Join our development discord: https://discord.gg/PF8QdvF",
		"RydeScape is a work in progress, if you have suggestions please share them",
		"You can zoom by holding CTRL and scrolling the mouse wheel",
		"Type ::commands in chat to see a list of commands",
		"RydeScape is free, open source and community driven",
		"RydeScape is independent and not affiliated with RuneScape or JAGEX",
		"RydeScape is free for all, if you paid for anything, you were scammed",
	};
	
	/**
	 * Declares all the announcements
	 */
	public static void declare() {
		for (int i = 0; i < ANNOUNCEMENTS.length; i++) {
			announcements.add(ANNOUNCEMENTS[i]);
		}

		if (RydeConstants.EXPERIMENTS.BASIC_ECONOMY.getValue()) {
			announcements.add("The economy is limited at the moment. Some items may change to ash.");
		}

		if (RydeConstants.EXPERIMENTS.BASIC_MOBS.getValue()) {
			announcements.add("NPC spawns are limited at the moment. Join our discord for more info.");
		}

		if (RydeConstants.EXPERIMENTS.BOOSTED_ACCOUNT.getValue()) {
			announcements.add("Boosted accounts are available with 3.33x Exp gain.");
		}

		if (RydeConstants.EXPERIMENTS.DISABLE_TRADABLE_ITEMS.getValue()) {
			announcements.add("Some items are untradable while we develop the economy.");
		}

		if (RydeConstants.EXPERIMENTS.NEW_POUCH_PAYMENT.getValue()) {
			announcements.add("All payments will be taken from your pouch before your inventory.");
		}

		if (RydeConstants.EXPERIMENTS.NEW_SKILL_WRAPPER.getValue()) {
			announcements.add("A new skill system is live. Please report issues to our discord.");
		}

		logger.info(Utility.format(announcements.size()) + " Announcements have been loaded successfully.");
	}

	/**
	 * Initializes the task
	 */
	public static void initialize() {
		TaskQueue.queue(new Task(250, false) {
			@Override
			public void execute() {
				final String announcement = Utility.randomElement(announcements);
				World.sendGlobalMessage(iconNumber + newsColor + " " + announcement);
			}

			@Override
			public void onStop() {
			}
		});
	}
	
}
