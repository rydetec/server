package com.base.rs2.content.interfaces.impl;

import com.base.rs2.content.interfaces.InterfaceHandler;
import com.base.rs2.entity.player.Player;

/**
 * Handles the training teleport interface
 * @author Daniel
 *	Misthalin
 */
public class TrainingInterface extends InterfaceHandler {
	
	public TrainingInterface(Player player) {
		super(player);
	}

	private final String[] text = {
			//"Cows",
			//"Rock Crabs",
			//"Hill Giants",
			//"Al-Kharid",
			//"Yaks",
			//"Brimhaven Dung",
			//"Taverly Dung",
			//"Slayer Tower",
			//"Lava Dragons",
			//"Mithril Dragons",
			"Lumbridge",
			"Varrock",
			"Edgeville",
			"Barbarian Village",
			"Draynor",
			"Al Kharid"
	};

	@Override
	protected String[] text() {
		return text;
	}

	@Override
	protected int startingLine() {
		return 61051;
	}

}

