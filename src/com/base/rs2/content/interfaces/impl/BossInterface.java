package com.base.rs2.content.interfaces.impl;

import com.base.rs2.content.interfaces.InterfaceHandler;
import com.base.rs2.entity.player.Player;

/**
 * Handles the boss teleport interface
 * @author Daniel
 * Kandarin
 */
public class BossInterface extends InterfaceHandler {
	
	public BossInterface(Player player) {
		super(player);
	}

	private final String[] text = {
			//"King Black Dragon",
			//"Sea Troll Queen",
			//"Barrelchest",
			//"Corporeal Beast",
			//"Daggonoths Kings",
			//"Godwars",
			//"Zulrah",
			//"Kraken",
			//"Giant Mole",
			//"Chaos Element",
			//"Callisto",
			//"Scorpia",
			//"Vet'ion",
			//"Venenatis (N/A)",
			//"Chaos Fanatic",
			//"Crazy archaeologist",
			//"Kalphite Queen (N/A)",
			"Catherby",
			"Seer's Village",
			"West Ardougne",
			"East Ardougne",
			"Gnome Tree Village",
			"Fishing Guild",
			"Barbarian Outpost",
			"Baxtorian Falls",
			"Port Khazard",
	};

	@Override
	protected String[] text() {
		return text;
	}

	@Override
	protected int startingLine() {
		return 64051;
	}

}

