package com.base.rs2.content.interfaces.impl;

import com.base.rs2.content.interfaces.InterfaceHandler;
import com.base.rs2.entity.player.Player;

/**
 * Handles the minigame teleport interface
 * @author Daniel
 * Dungeons
 */
public class MinigameInterface extends InterfaceHandler {
	
	public MinigameInterface(Player player) {
		super(player);
	}

	private final String[] text = {
			//"Barrows",
			//"Warriors Guild",
			//"Duel Arena",
			//"Pest Control",
			//"Fight Caves",
			//"Weapon Game",
			//"Clan Wars",
			"Edgeville",
			"Varrock",
			"Security Stronghold",
			
	};

	@Override
	protected String[] text() {
		return text;
	}

	@Override
	protected int startingLine() {
		return 65051;
	}

}

