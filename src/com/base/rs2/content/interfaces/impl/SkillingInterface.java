package com.base.rs2.content.interfaces.impl;

import com.base.rs2.content.interfaces.InterfaceHandler;
import com.base.rs2.entity.player.Player;

/**
 * Handles the skilling teleport interface
 * @author Daniel
 * Asgarnia
 */
public class SkillingInterface extends InterfaceHandler {
	
	public SkillingInterface(Player player) {
		super(player);
	}

	private final String[] text = {
			//"Wilderness Resource",
			//"Thieving",
			//"Crafting",
			//"Agility",
			//"Mining",
			//"Smithing",
			//"Fishing",
			//"Woodcutting",
			//"Farming",
			"Falador",
			"Rimmington",
			"Taverley",
			"Port Sarim",
			"Goblin Village"
			
	};

	@Override
	protected String[] text() {
		return text;
	}

	@Override
	protected int startingLine() {
		return 62051;
	}

}

