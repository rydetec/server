package com.base.rs2.content.interfaces.impl;

import com.base.rs2.content.interfaces.InterfaceHandler;
import com.base.rs2.entity.player.Player;

/**
 * Handles the other teleport interface
 * @author Daniel
 *
 */
public class OtherInterface extends InterfaceHandler {
	
	public OtherInterface(Player player) {
		super(player);
	}

	private final String[] text = {
			"Entrana",
			"Crandor",
			"Karamja",
			"Duel Arena",
			"Brimhaven",
			"Testing Area",
			"Random",
			"NPC Test Zone",
			//"Relaxation Zone",

			
	};

	@Override
	protected String[] text() {
		return text;
	}

	@Override
	protected int startingLine() {
		return 61551;
	}

}

