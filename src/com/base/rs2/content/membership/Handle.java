package com.base.rs2.content.membership;

import com.base.rs2.entity.player.Player;

/**
 * @author Daniel
 */
public interface Handle {

	public void handle(Player player);

}
