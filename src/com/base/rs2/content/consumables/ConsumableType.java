package com.base.rs2.content.consumables;

public enum ConsumableType {
	POTION, FOOD;
}
