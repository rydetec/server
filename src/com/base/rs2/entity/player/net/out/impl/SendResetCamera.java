package com.base.rs2.entity.player.net.out.impl;

import com.base.core.network.StreamBuffer;
import com.base.rs2.entity.player.net.Client;
import com.base.rs2.entity.player.net.out.OutgoingPacket;

public class SendResetCamera extends OutgoingPacket {

	@Override
	public void execute(Client client) {
		StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(1);
		out.writeHeader(client.getEncryptor(), 107);
		client.send(out.getBuffer());
	}

	@Override
	public int getOpcode() {
		return 107;
	}

}
