package com.base.rs2.entity.player.net.out.impl;

import com.base.core.network.StreamBuffer;
import com.base.core.network.StreamBuffer.OutBuffer;
import com.base.rs2.entity.player.net.Client;
import com.base.rs2.entity.player.net.out.OutgoingPacket;

public class SendEnterString extends OutgoingPacket {

	@Override
	public void execute(Client client) {
		OutBuffer outBuffer = StreamBuffer.newOutBuffer(5);
		outBuffer.writeHeader(client.getEncryptor(), getOpcode());
		client.send(outBuffer.getBuffer());
	}

	@Override
	public int getOpcode() {
		return 187;
	}

}
