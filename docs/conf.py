import re

import sphinx

from recommonmark.parser import CommonMarkParser

source_parsers = {
    '.md': CommonMarkParser,
}

source_suffix = ['.rst', '.md']

project = 'RydeScape Server'
author = 'RydeTec'
copyright = '2020 RydeTec'
version = '0.1'
release = version

html_sidebars = {
   '**': ['globaltoc.html', 'sourcelink.html', 'searchbox.html']
}
